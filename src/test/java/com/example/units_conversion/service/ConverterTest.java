package com.example.units_conversion.service;

import com.example.units_conversion.service.model.SIConversion;
import com.example.units_conversion.service.service.Converter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConverterTest {

	@Autowired
	private Converter converter;

	@Test
	public void itConvertsExpression() {

		SIConversion converted = converter.parseWidelyUsedExpression("degree/minute");
		assertThat(converted.getUnit_name()).isEqualTo("rad/s");
		assertThat(converted.getMultiplication_factor()).isEqualTo(0.00029088820866572);

		converted = converter.parseWidelyUsedExpression("(degree/(minute*hectare))");
		assertThat(converted.getUnit_name()).isEqualTo("(rad/(s*m²))");
		assertThat(converted.getMultiplication_factor()).isEqualTo(0.000000029088820866572);

	}

	@Test(expected = IllegalArgumentException.class)
	public void itThrowsWhenEmpty() {
		converter.parseWidelyUsedExpression("");
	}

	@Test(expected = IllegalArgumentException.class)
	public void itThrowsWhenNull() {
		converter.parseWidelyUsedExpression(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void itThrowsWhenNoUnits() {
		converter.parseWidelyUsedExpression("2.0 * 1.0");
	}

}
