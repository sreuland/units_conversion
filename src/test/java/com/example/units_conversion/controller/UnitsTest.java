package com.example.units_conversion.controller;

import com.example.units_conversion.service.UnitsConversionApplication;
import com.example.units_conversion.service.controller.Units;
import com.example.units_conversion.service.model.SIConversion;
import com.example.units_conversion.service.service.Converter;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(controllers = Units.class)
@DirtiesContext(classMode= ClassMode.AFTER_CLASS)
@ContextConfiguration(classes = UnitsConversionApplication.class)
public class UnitsTest {

    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
    }

    @MockBean
    private Converter converter;

    @Test
    public void itShouldConvert() throws Exception {

        SIConversion conversion = new SIConversion("rad/s", 0.00029088820866572);

        doReturn(conversion).when(converter).parseWidelyUsedExpression(eq("degree/minute"));
        mockMvc.perform(MockMvcRequestBuilders
                .get("/units/si")
                .param("units", "degree/minute"))
                .andDo(document("convert-example",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        requestParameters(parameterWithName("units").description("A string containing any number of SI units " +
                                "multiplied or divided, which might contain parenthesis")),
                        responseFields(fieldWithPath("unit_name").description("A string of units converted to their SI counterpart. " +
                                "They do not need to be reduced."),
                                fieldWithPath("multiplication_factor").description("A floating point number (with 14 significant digits) you can " +
                                        "use to convert any input in the original units to the new widely-used SI units."))))
                .andExpect(jsonPath("unit_name", is("rad/s")))
                .andExpect(jsonPath("multiplication_factor", is(0.00029088820866572)))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void itShouldReportError() throws Exception {

        doThrow(IllegalArgumentException.class).when(converter).parseWidelyUsedExpression(eq("unknown/minute"));
        mockMvc.perform(MockMvcRequestBuilders
                .get("/units/si")
                .param("units", "unknown/minute"))
                .andDo(document("convert-example-error"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());

    }
}
