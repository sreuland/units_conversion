package com.example.units_conversion.service;

import com.example.units_conversion.service.model.ConfigProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({ConfigProperties.class})
/**
 * Main entry point for the application.
 */
public class UnitsConversionApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnitsConversionApplication.class, args);
	}
}
