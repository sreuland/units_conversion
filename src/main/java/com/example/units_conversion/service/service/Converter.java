package com.example.units_conversion.service.service;

import com.example.units_conversion.service.model.ConfigProperties;
import com.example.units_conversion.service.model.ConfigProperties.SIUom;
import com.example.units_conversion.service.model.SIConversion;
import org.mariuszgromada.math.mxparser.Expression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Arrays.stream;
import static org.springframework.util.ObjectUtils.isEmpty;

/**
 * Domain service to perform conversions
 */
@Service
public class Converter {

    @Autowired
    private ConfigProperties configProps;

    /**
     * convert a widely used units expression into SI units expression
     *
     * @param exp the widely used unit expression
     * @return the SI unit expression or throws if invalid request
     */
    public SIConversion parseWidelyUsedExpression(String exp) {
        checkArgument(!isEmpty(exp));
        String[] converted = {escapeVariableNames(exp)};
        Expression e = new Expression(converted[0]);

        String[] missingArgs = e.getMissingUserDefinedArguments();
        if (isEmpty(missingArgs)) {
            // the expression has no unit terms, so assume not valid
            throw new IllegalArgumentException();
        }

        stream(missingArgs).forEach(missingArg -> {
            SIUom siUom = getSIUom(missingArg);
            e.defineArgument(missingArg, siUom.getValue());
            converted[0] = converted[0].replaceAll(missingArg, siUom.getLabel());
        });

        return new SIConversion(converted[0], roundToSignificant(e.calculate(), 14));
    }

    private SIUom getSIUom(String widelyUsedTerm) {
        if (!configProps.getWidelyUsedAlias().containsKey(widelyUsedTerm)) {
            throw new IllegalArgumentException("invalid argument - " + widelyUsedTerm);
        }
        SIUom siUom = configProps.getSiUnits().get(configProps.getWidelyUsedAlias().get(widelyUsedTerm));
        if (!configProps.getWidelyUsedAlias().containsKey(widelyUsedTerm)) {
            throw new IllegalArgumentException("invalid si argument - " + configProps.getWidelyUsedAlias().get(widelyUsedTerm));
        }
        return siUom;
    }

    // ugh, couldn't find an expression parser lib that correctly detected these non-alpha chars as arguments
    // manually convert here to something that is alpha so expression parser can work
    private String escapeVariableNames(String exp) {
        return exp.replaceAll("\\u00B0", "degreesymbol")
                .replaceAll("\\u0027", "minutesymbol")
                .replaceAll("\\u0022", "secondsymbol");
    }

    private double roundToSignificant(double value, int sigFigs) {
        MathContext mc = new MathContext(sigFigs, RoundingMode.HALF_UP);
        BigDecimal bigDecimal = new BigDecimal(value, mc);
        return bigDecimal.doubleValue();
    }
}
