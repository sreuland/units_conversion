package com.example.units_conversion.service.model;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

@ConfigurationProperties(prefix  = "units")
/**
 * Binding to config properties from .yml
 *
 */
public class ConfigProperties {

    private Map<String, String> widelyUsedAlias = newHashMap();
    private Map<String, SIUom> si = newHashMap();

    /**
     * binding to units.si
     * @return the si structure
     */
    public Map<String, SIUom> getSiUnits() {
        return si;
    }

    /**
     * binding to units.widelyUsedAlias
     * @return the widely used alias mapping to SI
     */
    public Map<String, String> getWidelyUsedAlias() {
        return widelyUsedAlias;
    }

    /**
     * binding to the units.SI.[key,value]
     */
    public static class SIUom {
        private String label;
        private double value;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public double getValue() {
            return value;
        }

        public void setValue(double value) {
            this.value = value;
        }
    }
}
