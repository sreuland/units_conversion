package com.example.units_conversion.service.model;

/**
 * State of a conversion
 */
public class SIConversion {

    private double multiplication_factor;
    private String expression;

    public SIConversion(String expression, double multiplication_factor) {
        this.expression = expression;
        this.multiplication_factor = multiplication_factor;
    }

    /**
     * get a floating point number (with 14 significant digits) for conversion factor
     *
     * @return floating point number
     */
    public double getMultiplication_factor() {
        return multiplication_factor;
    }

    /**
     * get the unit name
     *
     * @return unit name
     */
    public String getUnit_name() {
        return expression;
    }
}
