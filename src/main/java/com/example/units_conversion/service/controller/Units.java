package com.example.units_conversion.service.controller;

import com.example.units_conversion.service.model.SIConversion;
import com.example.units_conversion.service.service.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Endpoint controller for units conversion
 */
@RestController
@RequestMapping("/units")
public class Units {

    private static Logger logger = LoggerFactory.getLogger(Units.class);

    @Autowired
    private Converter converter;

    @RequestMapping(path = "/si", method = RequestMethod.GET)
    @ResponseBody
    /**
     * perform unit conversion to SI from “widely used” terms in an expression
     * @param incomingUnits incoming expression with widely used terms
     * @return converted expression
     */
    public ResponseEntity<SIConversion> convertSI(@RequestParam("units") String incomingUnits) {
        logger.info("Received convert request for {}", incomingUnits);

        try {
            return new ResponseEntity<>(converter.parseWidelyUsedExpression(incomingUnits), HttpStatus.OK);
        } catch (IllegalArgumentException ie) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}
