#Full Stack Challenge
This is a submission for the full stack challenge. It will perform unit conversion to SI from “widely used” counterparts.

##Requirements
Maven 3.3 and Java 8 are required to be available on your command line to build this project.

##Build:
from this directory:
mvn clean install

##Run the web service locally from this directory after build:
java -Dserver.port=8080 -jar target/units_conversion-0.0.1-SNAPSHOT.jar 

the web service will be available at the port specified, in this example, localhost:8080.
here's an example request to run:  
http://locahost:8080/units/si?units=degree/minute

##API Docs
http://locahost:8080/docs/api-guide.html

##Health checks
This service publishes utility endpoints that can be used for additional info/status:  
http://locahost:8080/actuator/info  
http://locahost:8080/actuator/health  

##IBM Cloud/CloudFoundry
There is a deployment available:  
https://si-unit-conversion.mybluemix.net/units/si?units=degree/minute  
https://si-unit-conversion.mybluemix.net/docs/api-guide.html  
https://si-unit-conversion.mybluemix.net/actuator/info  
https://si-unit-conversion.mybluemix.net/actuator/health  

Can also optionally use 'cf push' from this directory to deploy this app onto your CloudFoundry org/space. 
It will use the manifest.yml located in same directory for details. This requires you have installed the CF command line 
and have an account with a cloud provider.



